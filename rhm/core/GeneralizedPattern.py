from rhm.core import PatternStatistics as ps
import math


def to_int(char):
    """
    Converts char to its integer value.
    For char in [A-Z], char is matches to i such 'A'+i = char.
    else it is matches with lower case values

    :param char: character
    :type char: string of length 1
    :return: the corresponding int value
    :rtype: an int
    """
    return ord(char) - ord('A') if char <= 'Z' else 26 + (ord(char) - ord('a'))


class GeneralizedPattern(object):
    """
    Class to represent a pattern with generalized pattern, ie a
    pattern for which each value can be a range.
    As an example, a pattern can be [AC][BE]
    which means that the pattern is of length 2, and the first element
    can range from A to C and the second one range from B to E.
    """

    def __init__(self, p_min, p_max=None, stats=None):
        """
        Initialize GeneralizedPattern
        :param p_min: the minimal values
        :type p_min: a string
        :param p_max: the maximal values
        :type p_max: a string
        :param stats: a pattern statistics
        :type stats: PatternStatistics
        """
        if p_max is not None and len(p_min) != len(p_max):
            raise ValueError("length() of p_min an p_max should be identical {} vz {}"\
                             .format(len(p_min), len(p_max)))
        self._p_min = p_min
        self._p_max = p_max
        if stats is None:
            self._stats = ps.PatternStatistics()
        else:
            self._stats = stats

    def __eq__(self, other):
        """ Overloading of the equality operator.

        :param other: another pattern
        :type other: GeneralizedPattern
        :return: true if both patterns are of same length and the distance between both elements\
         (as define by self.dist) is 0.
        :rtype: Boolean
        """

        if len(self._p_min) != len(other.p_min):
            return False
        return True if self.dist(other) == 0 else False

    def __getitem__(self, idx):
        """return the ith item as a tupple (min, max)"""
        if self._p_max is None:
            return self._p_min[idx], None
        return (self._p_min[idx], self._p_max[idx])

    def __len__(self):
        """return the length of the pattern"""
        return len(self.p_min)

    def __repr__(self):
        """returns a string representing the patern and the its statistics"""
        return "{:<15} stats={}".format(self.__str__(), self._stats)

    def __str__(self):
        """returns a string representing the pattern"""
        if self.p_max is None:
            return self.p_min
        ret = []
        for x, y in zip(self._p_min, self._p_max):
            if x == y:
                ret.append(x)
            else:
                ret.append('[' + x + y + ']')
        return "".join(ret)

    @property
    def stats(self):
        """getter for the stats parameter"""
        return self._stats

    @stats.setter
    def stats(self, param_value):
        """setter for the stats parameter"""
        self._stats = param_value

    @property
    def p_min(self):
        """getter for p_min"""
        return self._p_min

    @p_min.setter
    def p_min(self, value):
        """setter for the p_min parameter (no check of consistency with p_max).
        :param value: the value of p_min
        :type value: a string
        """
        self._p_min = value

    @property
    def p_max(self):
        """"getter for p_max"""
        return self._p_max

    @p_max.setter
    def p_max(self, value):
        """setter for the p_max parameter (no check of consistency with p_max).
        :param value: the value to copy
        :type value: a string
        """
        self._p_max = value

    def interval_diff(self, m1, M1, m2, M2):
        """
        Computes the disancets between two interval.
        The distance is the following: if there is inclusion between
        intervals, the distance is 0. Else, the distance is Union(I1, I2) - Intersection(I1, I2).
        Let I1 and I2 be such that I1=[m1, M1] and I2=[m2, M2]
        Union (I1, I2) = |min(m1,m2)-max(M1,M2)|
        Inter (I1, I2) = |min(M1,M2)-max(m1,m2)|

        :param m1: lower bound of the first interval
        :type m1: number
        :param M1: upper bound of the first interval
        :type m1: number
        :param m2: lower bound of the first interval
        :type m2: number
        :param M2: upper bound of the first interval
        :type m2: number
        :return: the dissimilarity between the two intervals [m1, M1] and [m2, M2]
        :rtype: float
        """
        if (m1 <= m2) and (M2 <= M1) or (m2 <= m1) and (M1 <= M2):
            return 0.0
        union = abs(min(m1, m2) - max(M1, M2))
        intersection = min(M1, M2) - max(m1, m2)
        return math.pow(union - intersection, 2)

    def dist(self, other):
        """computes the distances between self and other.
        The distance is the following: if there is inclusion between
        intervals, the distance is 0. Else, the distance is Union(I1, I2) - Intersection(I1, I2).
        Let I1 and I2 be such that I1=[m1, M1] and I2=[m2, M2]
        Union (I1, I2) = |min(m1,m2)-max(M1,M2)|
        Inter (I1, I2) = |min(M1,M2)-max(m1,m2)|

        :param other: an other pattern
        :type other: GeneralizedPattern
        :return: the distance between self and other.
        :rtype: int

        """
        if self._p_max is None and other.p_max is None:
            return sum([abs(ord(i) - ord(j)) for i, j in zip(self.p_min, other.p_min)])
        ret = 0
        if self._p_max is None and other.p_max is not None:
            for _min1, _max1, _min2, _max2 in zip(self.p_min, self.p_min, other.p_min, other.p_max):
                ret += self.interval_diff(ord(_min1), ord(_max1), ord(_min2), ord(_max2))
            return ret

        if self.p_max is not None and other.p_max is None:
            for _min1, _max1, _min2, _max2 in zip(self.p_min, self.p_max, other.p_min, other.p_min):
                ret += self.interval_diff(ord(_min1), ord(_max1), ord(_min2), ord(_max2))
            return ret

        if self.p_max is not None and other.p_max is not None:
            for _min1, _max1, _min2, _max2 in zip(self.p_min, self.p_max, other.p_min, other.p_max):
                ret += self.interval_diff(ord(_min1), ord(_max1), ord(_min2), ord(_max2))
            return ret

    def compare_coverage(self, other):
        """
        Compare two GeneralizedPattern in term of coverage:
        [BC] completely falls in [AD]
        :param other: the pattern to compare with
        :return: boolean, -1 if self is covers more, 1 if other, 0 if they are equal
        """
        min_1 = self.p_min
        max_1 = self.p_max
        if self.p_max is None:
            max_1 = self.p_min
        min_2 = other.p_min
        max_2 = other.p_max
        if other.p_max is None:
            max_2 = other.p_min

        cov_1 = True
        cov_2 = True
        for a, b, c, d in zip(min_1, max_1, min_2, max_2):
            if not (a >= c and b <= d):
                cov_1 = False
            if not (c >= a and d <= b):
                cov_2 = False

        if cov_1:
            return -1
        elif cov_2:
            return 1
        else:
            return 0

    def to_json(self):
        return dict(_p_min=self._p_min, _p_max=self._p_max, _stats=self._stats)
