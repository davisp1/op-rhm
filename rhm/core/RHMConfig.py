
class RHMConfig(object):

    _help = '-b\t discretization param (number of  bins), can be a list\n' \
            '-c\t comma separated list of columns to use\n' \
            '-ct\t filter the data on a var:value based format\n' \
            '-d\t column delimeter (reading ucr files)\n' \
            '-f\t file format (json, pkl, ucr, ucrd)\n' \
            '-g\t invoke the generalization step\n' \
            '-i\t input data file (assume as json)\n' \
            '-j\t number of derivative to calculate\n' \
            '-k\t keep the good patterns (ts percentage) after tiling\n' \
            '-l\t comma separated list of classes/labels e.g. 1,2. Default=0,1\n' \
            '-n\t acceptable percentage  of noise\n' \
            '-nm\t comma separated list of variables names\n' \
            '-o\t output file\n' \
            '-t\t input test file (assume as json)\n' \
            '-ts\t minimum percentage of coverage\n' \
            '-w\t window length for pattern (can be a list separated by ,\n' \
            '-x\t window length for multi-var pattern (can be a list separated by ,\n' \
            '-y\t subset the time. start:end. Default=all. None for time end\n' \
            '-v\t Save data for the visu\n'

    def __init__(self):


        #Airbus 1
        ## Data preparation - USE IKATS API
        self.columns = [0,1,2]
        self.filter = None
        self.delimter = ''
        self.format = 'ikats'
        self.input_file = '../Data/Airbus/E1101_extended.json'
        self.derivative = 0
        self.names = None
        self.output = '/home/ikats/airbus_12'
        self.test = None
        self.data_subset = ''

        ##RHM call
        self.bins = [7]
        self.labels = None
        self.noise = 0.9
        self.cov_threshold = 0.2
        self.windows = [2, 3, 4]
        self.generalizer = False
        self.conf_threshold = 1

        ##Unimplemented by IKATS
        self.pair_windows = []
        self.vis = False


        """
        #Airbus 2
        self.bins = [12, 12]
        self.columns = np.array([3, 7])
        self.filter = '0:40,1:-0.174:gr,1:0.174'
        self.delimter = ','
        self.format = 'json'
        self.generalizer = False
        self.input_file = '../Data/Airbus/E1031_new_var.json'
        self.derivative = 0
        self.labels = {'0','3'}         #list of strings
        self.noise = 0.9
        self.names = ['Speed','Ang_dr']
        self.output = '/home/airbus_2_w2'
        self.test = ''
        self.cov_threshold = 0.05
        self.conf_threshold = 1
        self.windows = [2]
        self.pair_windows = [2]
        self.data_subset = '340:None'
        self.vis = False
        """

        """
        #UCR
        self.bins = [4, 4]
        self.columns = [0, 1]
        self.filter = None
        self.delimter = ','
        self.format = 'ucrd'
        self.generalizer = False
        self.input_file = '../Data/RHM_UCR/Trace_TRAIN'
        self.derivative = 1
        self.conf_threshold = 1
        self.labels = None
        self.noise = 0.9
        self.names = ['Tr', 'Tr_dr']
        self.output = 'Trace'
        self.test = '../Data/RHM_UCR/Trace_TEST'
        self.cov_threshold = 0.05
        self.windows = [4]
        self.pair_windows = [4]
        self.data_subset = None
        self.vis = False
        """