# Changelog
### [3.3] - 2018-01-17

### Added
- changelog.md file

### Changed

#### Ikats
- Read table with ikats api
- Update Sql file to be compliant with current data model
